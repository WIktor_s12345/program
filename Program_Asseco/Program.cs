﻿using System;
using System.IO;
using Microsoft.Office.Interop.Word;


namespace Program_Asseco
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Clear();

            //sciezka do pliku zip lub zawierajacego AOM
            string dirAOM;
            //sciezka do pliku z zakresem informacyjnym
            string dirINF;
            //wybrane wymaganie
            string REQ;
            //sciezka do katalogu docelowego gdzie powinny zostac skopiowane artefakty dotyczace podanego wymagania
            string copyDIR;

            Console.WriteLine("wprowadzanie danych");



            //wprowadzanie danych do programu
            Console.WriteLine("podaj sciezke do aom");
            //  dirAOM = "@" + Console.ReadLine();
            dirAOM = @"C:\Users\Wiktor\Documents\Rainmeter\wiktor\czesc.docx";


            Console.WriteLine("podaj sciezke z zakresem informacyjnym");
            dirINF = "@" + Console.ReadLine();
          
            Console.WriteLine("podaj wymaganie");
            REQ = Console.ReadLine();
          
            Console.WriteLine("podaj sciezke katalogu do kopiowania");
            // copyDIR = "@" + Console.ReadLine();
            copyDIR = @"C:\Users\Wiktor\Documents\Rainmeter\wktor";
            ///sprawdzanie poprawnosci danych
            while (dirAOM.Length == 0) {
                Console.WriteLine("sprawdz dane pliku zip");
                dirAOM = Console.ReadLine();

            }

            while (dirINF.Length == 0)
            {
                Console.WriteLine("sprawdz dane informacyjne");
                dirINF = Console.ReadLine();

            }

            while (REQ.Length == 0)
            {
                Console.WriteLine("sprawdz wymaganie");
                REQ = Console.ReadLine();

            }

            while (copyDIR.Length == 0)
            {
                Console.WriteLine("sprawdz dane katalogu kopiowania");
                copyDIR = Console.ReadLine();

            }
            //////////////////////////////
            Console.WriteLine(copyDIR);
            try
            {


                if (Directory.Exists(copyDIR))
                {
                    Console.WriteLine("sciezka juz istnieje");
                    return;
                }
                DirectoryInfo di = Directory.CreateDirectory(copyDIR);
                Console.WriteLine("katalog zostal utworzony");
            }

            catch (Exception e) { }


            kopiowanie_Word(dirAOM, copyDIR);

           
            
            
            
            
            Console.WriteLine("koniec");
            Console.ReadKey();
        
        }


        public static void kopiowanie_Word(string SourcePath, string TargetPath)
        {
            string fileToCopy = SourcePath;
            string destinationDirectory = TargetPath;

            File.Copy(fileToCopy, destinationDirectory + Path.GetFileName(fileToCopy));
        }
       
        
        public static void szukanie_w_pliku (string copydir, string req) 
        {

            object fileName = "@" + copydir + @"\skopiowanyAOM.docx"; 
            string textToFind = req; 
            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = new Microsoft.Office.Interop.Word.Document();
            object missing = System.Type.Missing;


            try
            {
                doc = word.Documents.Open(ref fileName, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref missing, ref missing,
                ref missing, ref missing);
                doc.Activate();
                foreach (Microsoft.Office.Interop.Word.Range docRange in doc.Words)
                {
                    if (docRange.Text.Trim().Equals(textToFind,
                        StringComparison.CurrentCultureIgnoreCase))
                    {
                        docRange.HighlightColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdDarkYellow;
                        docRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdWhite;
                    }
                    //Response.Write(docRange.Text);
                    else { Console.WriteLine("podana wlasciwosc nie istnieje"); }
                }
            }
            catch (Exception ex)
            {
               // Response.Write("Error : " + ex.Message);
            }
        }

    
    
    
    
    }
}

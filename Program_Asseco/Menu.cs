﻿using System;

namespace Program_Asseco
{
    class Menu {
        string[] elementy;
        int x, y;

        public Menu(string[] elementyMenu, int x, int y)
        {
            elementy = elementyMenu;
            this.x = x;
            this.y = y;

        }

        public int Wyswietl()
        {
            int wybrany = 0;
            Console.CursorVisible = false;
            ConsoleKeyInfo k;
            Console.BackgroundColor = ConsoleColor.DarkGray;

            do
            {   //kolory

                Console.SetCursorPosition(x, y);
                for (int i = 0; i < elementy.Length; i++)
                {

                    if (wybrany == i) Console.ForegroundColor = ConsoleColor.Red;
                    else Console.ForegroundColor = ConsoleColor.Green;
                    Console.CursorLeft = x;


                    Console.WriteLine(elementy[i].PadRight(30));
                }

                k = Console.ReadKey(true);
                //wybieranie
                if (k.Key == ConsoleKey.UpArrow && wybrany > 0)
                {
                    wybrany--;
                }
                else if (k.Key == ConsoleKey.DownArrow && wybrany < elementy.Length - 1)
                {
                    wybrany++;
                }
                else if (k.Key == ConsoleKey.Escape) wybrany = 0;


            } while (!(k.Key == ConsoleKey.Enter));

            // Console.ResetColor();

            Console.CursorVisible = true;

            return wybrany;
        }




    }
}
